#ifndef GUI_HPP
#define GUI_HPP

#include <QMainWindow>

namespace Ui
{
    class GUI;
}

class GUI : public QMainWindow
{
    Q_OBJECT

    public:
    explicit GUI(QWidget *parent = 0);
    ~GUI();

    private:
    Ui::GUI *ui;
};

#endif // GUI_HPP
