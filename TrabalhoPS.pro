
QT          +=  core gui widgets

TARGET       =  TrabalhoPS
TEMPLATE     =  app


SOURCES     +=  main.cpp\
                gui.cpp \
                machine.cpp \
                macroprocessor.cpp \
                assembler.cpp

HEADERS     +=  gui.hpp \
                machine.hpp \
                macroprocessor.hpp \
                assembler.hpp \
                global.hpp

FORMS       +=  gui.ui
